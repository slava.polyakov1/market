﻿using Market.Discounts;
using Market.Models;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Market.Tests
{
    public static class UnitTestsHelper
    {
        public static void SeedDiscount()
        {
            Type storeProductsType = typeof(StoreProducts);
            BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Static;
            var productsProp = storeProductsType.GetProperty("InitProducts", bindingAttr);
            var productsWithDiscount = new ProductBase[]
            {
                new ProductWholesale
                (
                    3.00,
                    3,
                    1.25,
                    "A",
                    new FixDiscount(14.25)
                ),
                new ProductBase
                (
                    4.25,
                    "B",
                    new PercentageDiscount(0.25)
                ),
                new ProductWholesale
                (
                    5.00,
                    6,
                    1,
                    "C",
                    new PercentageDiscount(0.1)
                ),
                new ProductBase
                (
                    0.75,
                    "D",
                    new FixDiscount(0.25)
                )
            };
            productsProp.SetValue(storeProductsType, productsWithDiscount);
        }
    }
}
