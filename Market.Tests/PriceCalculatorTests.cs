﻿using Market.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Market.Tests
{
    public class PriceCalculatorTests
    {
        [SetUp]
        public void Setup()
        {
            StoreProducts.ClearAll();
        }

        [Test]
        [TestCase("ABCDABA", 9.25)]
        [TestCase("CCCCCCC", 0)]
        [TestCase("ABCD", 5)]
        public void PriceCalculator_CalculateProductBase_GetPriceOfProductBaseOnly(string initValues, double expected)
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();
            PriceCalculator priceCalculator = new PriceCalculator();

            foreach (var initValue in initValues)
            {
                pointOfSaleTerminal.Scan(initValue.ToString());
            }
            Type priceCalculatorType = priceCalculator.GetType();
            Type terminalType = pointOfSaleTerminal.GetType();
            BindingFlags bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance;
            MethodInfo method = priceCalculatorType.GetMethod("CalculateProductBase", bindingAttr);

            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = productsProp.GetValue(pointOfSaleTerminal);
            var actual = method.Invoke(priceCalculator, new object[] { products });

            Assert.AreEqual(expected, actual);
        }
        [Test]
        [TestCase("ABCDABA", 4)]
        [TestCase("CCCCCCC", 6)]
        [TestCase("ABCD", 2.25)]
        public void PriceCalculator_CalculateProductWholesalePrice_GetPriceOfProductWholesaleOnly(string initValues, double expected)
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();
            PriceCalculator priceCalculator = new PriceCalculator();

            foreach (var initValue in initValues)
            {
                pointOfSaleTerminal.Scan(initValue.ToString());
            }
            Type priceCalculatorType = priceCalculator.GetType();
            Type terminalType = pointOfSaleTerminal.GetType();
            BindingFlags bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance;
            MethodInfo method = priceCalculatorType.GetMethod("CalculateProductWholesalePrice", bindingAttr);

            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = (List<ProductBase>)productsProp.GetValue(pointOfSaleTerminal);
            var actual = method.Invoke(priceCalculator, new object[] { products });

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PriceCalculator_CalculateProductBase_GetPriceOfProductBaseOnlyWithDiscount()
        {
            UnitTestsHelper.SeedDiscount();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();
            PriceCalculator priceCalculator = new PriceCalculator();

            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.Scan("B");
            pointOfSaleTerminal.Scan("C");

            Type priceCalculatorType = priceCalculator.GetType();
            Type terminalType = pointOfSaleTerminal.GetType();
            BindingFlags bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance;
            MethodInfo method = priceCalculatorType.GetMethod("CalculateProductBase", bindingAttr);

            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = productsProp.GetValue(pointOfSaleTerminal);
            var actual = method.Invoke(priceCalculator, new object[] { products });

            double expected = 3.1875;

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PriceCalculator_CalculateProductWholesalePrice_GetPriceOfProductWholesaleOnlyWithDiscount()
        {
            UnitTestsHelper.SeedDiscount();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();
            PriceCalculator priceCalculator = new PriceCalculator();

            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.Scan("B");
            pointOfSaleTerminal.Scan("C");

            Type priceCalculatorType = priceCalculator.GetType();
            Type terminalType = pointOfSaleTerminal.GetType();
            BindingFlags bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance;
            MethodInfo method = priceCalculatorType.GetMethod("CalculateProductWholesalePrice", bindingAttr);

            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = productsProp.GetValue(pointOfSaleTerminal);
            var actual = method.Invoke(priceCalculator, new object[] { products });

            double expected = -12.1;

            Assert.AreEqual(expected, actual);
        }
    }
}
