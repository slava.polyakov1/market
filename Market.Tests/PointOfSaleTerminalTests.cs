﻿using Market.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Market.Tests
{
    public class PointOfSaleTerminalTests
    {
        [SetUp]
        public void Setup()
        {
            StoreProducts.ClearAll();
        }
        [Test]
        [TestCase("ABCDABA", 13.25)]
        [TestCase("CCCCCCC", 6.00)]
        [TestCase("ABCD", 7.25)]
        public void PointOfSaleTerminal_CalculateTotal_GetPriceTotal(string initValues, double expected)
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();

            foreach (var initValue in initValues)
            {
                pointOfSaleTerminal.Scan(initValue.ToString());
            }
            double actual = pointOfSaleTerminal.CalculateTotal();

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PointOfSaleTerminal_Scan_AddOneProduct()
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();

            pointOfSaleTerminal.Scan("A");
            Type terminalType = pointOfSaleTerminal.GetType();
            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = (List<ProductBase>)productsProp.GetValue(pointOfSaleTerminal);

            Assert.AreEqual(products.Count, 1);
        }
        [Test]
        public void PointOfSaleTerminal_SetPrice_ChangePriceOfProductBase()
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();

            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.Scan("B");
            pointOfSaleTerminal.Scan("C");
            pointOfSaleTerminal.SetPrice("B", 100);
            Type terminalType = pointOfSaleTerminal.GetType();
            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = (List<ProductBase>)productsProp.GetValue(pointOfSaleTerminal);
            var product = products.FirstOrDefault(p => p.Code == "B");

            Assert.AreEqual(product.Price, 100);
        }
        [Test]
        public void PointOfSaleTerminal_SetPrice_ChangePriceOfProductWholesale()
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();

            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.Scan("B");
            pointOfSaleTerminal.Scan("C");
            pointOfSaleTerminal.SetPrice(24, 12, 50, "C");
            Type terminalType = pointOfSaleTerminal.GetType();
            var productsProp = terminalType.GetField("Products", BindingFlags.NonPublic | BindingFlags.Instance);
            var products = (List<ProductBase>)productsProp.GetValue(pointOfSaleTerminal);
            var product = products.FirstOrDefault(p => p.Code == "C") as ProductWholesale;

            bool result = product.VolumePrice == 24 && product.VolumePricePerUnit == 12 && product.Price == 50 && product.Code == "C";
            Assert.IsTrue(result);
        }
    }
}
