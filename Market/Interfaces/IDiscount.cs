﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Interfaces
{
    public interface IDiscount
    {
        double ApplyDiscount(double productPrice);
    }
}
