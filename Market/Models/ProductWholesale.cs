﻿using Market.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Models
{
    public class ProductWholesale : ProductBase
    {
        public double VolumePrice { get; private set; }
        public int VolumePricePerUnit { get; private set; }
        public ProductWholesale(double volumePrice, int volumePricePerUnit, double price, string code) : base(price, code)
        {
            VolumePrice = volumePrice;
            VolumePricePerUnit = volumePricePerUnit;
        }
        public ProductWholesale(double volumePrice, int volumePricePerUnit, double price, string code, IDiscount appliedDiscount) : base(price, code, appliedDiscount)
        {
            VolumePrice = volumePrice;
            VolumePricePerUnit = volumePricePerUnit;
        }
        public void SetPrice(double volumePrice, int volumePricePerUnit, double price)
        {
            this.VolumePrice = volumePrice;
            this.VolumePricePerUnit = volumePricePerUnit;
            base.Price = price;
        }
    }
}
