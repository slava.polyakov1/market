﻿using Market.Discounts;
using Market.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Models
{
    public class ProductBase
    {
        public double Price { get; protected set; }
        public string Code { get; protected set; }
        public IDiscount AppliedDiscount { get; protected set; } = new NoDiscount();
        public ProductBase(double price, string code)
        {
            Price = price;
            Code = code;
        }
        public ProductBase(double price, string code, IDiscount appliedDiscount)
        {
            Price = price;
            Code = code;
            AppliedDiscount = appliedDiscount;
        }
        public void SetPrice(double price)
        {
            Price = price;
        }
    }
}
