﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market
{
    public static class StoreProducts
    {
        public static ProductBase[] InitProducts { get; private set; }
        public static void Seed()
        {
            InitProducts = new ProductBase[]
            {
                new ProductWholesale
                (
                    3.00,
                    3,
                    1.25,
                    "A"
                ),
                new ProductBase
                (
                    4.25,
                    "B"
                ),
                new ProductWholesale
                (
                    5.00,
                    6,
                    1,
                    "C"
                ),
                new ProductBase
                (
                    0.75,
                    "D"
                )
            };
        }
        public static void ClearAll()
        {
            InitProducts = null;
        }
    }
}
