﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Market
{
    public class PriceCalculator
    {
        public double CalculateTotal(List<ProductBase> products)
        {
            double totalWholesaleProducts = CalculateProductWholesalePrice(products);
            double totalBaseProducts = CalculateProductBase(products);
            return totalWholesaleProducts + totalBaseProducts;
        }
        private double CalculateProductBase(List<ProductBase> products)
        {
            double result = 0;
            var productsList = products.Where(p => p.GetType() == typeof(ProductBase));
            foreach (var product in productsList)
            {
                result += product.AppliedDiscount.ApplyDiscount(product.Price);
            }
            return result;
        }
        private double CalculateProductWholesalePrice(List<ProductBase> products)
        {
            double result = 0;
            List<ProductWholesale> wholesaleProductsList = products.Where(p => p is ProductWholesale).ToList().GroupBy(x => x.Code).Select(x => x.First()).ToList().ConvertAll(x => (ProductWholesale)x);
            foreach (var wholesaleProduct in wholesaleProductsList)
            {
                int wholesaleProductTotal = products.Count(p => p.Code == wholesaleProduct.Code);
                int volumeUnits = wholesaleProductTotal / wholesaleProduct.VolumePricePerUnit;
                double totalVolumePrice = wholesaleProduct.VolumePrice * volumeUnits;
                int otherUnits = wholesaleProductTotal - volumeUnits * wholesaleProduct.VolumePricePerUnit;
                double totalOtherPrice = wholesaleProduct.Price * otherUnits;
                result += wholesaleProduct.AppliedDiscount.ApplyDiscount(totalVolumePrice + totalOtherPrice);
            }
            return result;
        }
    }
}
