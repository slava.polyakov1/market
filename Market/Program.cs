﻿using System;

namespace Market
{
    class Program
    {
        static void Main(string[] args)
        {
            StoreProducts.Seed();
            PointOfSaleTerminal pointOfSaleTerminal = new PointOfSaleTerminal();
            pointOfSaleTerminal.SetPrice("A", 10.42);
            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.Scan("B");
            pointOfSaleTerminal.Scan("C");
            pointOfSaleTerminal.Scan("D");
            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.Scan("B");
            pointOfSaleTerminal.Scan("A");
            pointOfSaleTerminal.CalculateTotal();
        }
    }
}
