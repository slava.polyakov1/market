﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Market
{
    public class PointOfSaleTerminal
    {
        private List<ProductBase> Products = new List<ProductBase>();
        private readonly PriceCalculator priceCalculator;
        public PointOfSaleTerminal()
        {
            priceCalculator = new PriceCalculator();
        }
        public void SetPrice(string productCode, double price)
        {
            var product = StoreProducts.InitProducts.FirstOrDefault(p => p.Code == productCode);
            product.SetPrice(price);
        }
        public void SetPrice(double volumePrice, int volumePricePerUnit, double price, string productCode)
        {
            var product = StoreProducts.InitProducts.OfType<ProductWholesale>().FirstOrDefault(p => p.Code == productCode);
            product.SetPrice(volumePrice, volumePricePerUnit, price);
        }
        public void Scan(string productCode)
        {
            var scannedProduct = StoreProducts.InitProducts.FirstOrDefault(p => p.Code == productCode);
            if (scannedProduct == null)
                return;

            Products.Add(scannedProduct);
        }
        public double CalculateTotal()
        {
            return priceCalculator.CalculateTotal(Products);
        }
    }
}
