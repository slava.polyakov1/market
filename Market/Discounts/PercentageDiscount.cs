﻿using Market.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Discounts
{
    public class PercentageDiscount : IDiscount
    {
        public double PercentDiscount { get; set; }
        public PercentageDiscount(double percentDiscount)
        {
            PercentDiscount = percentDiscount;
        }
        public double ApplyDiscount(double productPrice) => productPrice - (productPrice * PercentDiscount);
    }
}
