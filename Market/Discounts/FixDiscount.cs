﻿using Market.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Discounts
{
    public class FixDiscount : IDiscount
    {
        public double FixPriceDiscount { get; set; }
        public FixDiscount(double fixPriceDiscount)
        {
            FixPriceDiscount = fixPriceDiscount;
        }
        public double ApplyDiscount(double productPrice) => productPrice - FixPriceDiscount;
    }
}
