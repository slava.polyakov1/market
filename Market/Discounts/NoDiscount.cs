﻿using Market.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Discounts
{
    public class NoDiscount : IDiscount
    {
        public double ApplyDiscount(double productPrice) => productPrice;
    }
}
